package states;


import handlers.GameStateManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.utils.TimeUtils;

import entities.Player;
import entities.Shoot;
import entities.Tank;

public class PlaySingle extends GameState {

	
	private Player player;
	private Tank tank;
	private OrthogonalTiledMapRenderer renderer;
	private OrthographicCamera cam;
	private TiledMap map;
	private Shoot shoot;
	
	private float lastShoot = 0f;
	private float moveTimer = 0f;
	private float Timer = 0f;
//	private double shotFreq  = TimeUtils.millisToNanos(3000);
	public PlaySingle(GameStateManager gSManager) {
		super(gSManager);
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		cam = new OrthographicCamera();
		cam.setToOrtho(false,w,h);
		cam.update();
		
		map = new TmxMapLoader().load("levels/level1.tmx");
		renderer = new OrthogonalTiledMapRenderer(map);
		
		cam.position.set(w/2,h/2,0);
		
		shoot = new Shoot(sb,(TiledMapTileLayer)map.getLayers().get(0), map);
		player = new Player(sb,(TiledMapTileLayer)map.getLayers().get(0));
		tank  = new Tank(sb);
	
	}

	@Override
	public void handleInput() {
		// TODO Auto-generated method stub
		if (Gdx.input.isKeyPressed(Keys.UP))
		{
			player.setUp();
		}
		else if (Gdx.input.isKeyPressed(Keys.DOWN))
		{
			player.setDown();
		}
		else if (Gdx.input.isKeyPressed(Keys.LEFT))
		{
			player.setLeft();
		}
		else if (Gdx.input.isKeyPressed(Keys.RIGHT))
		{
			player.setRight();
		}
	
		if (Gdx.input.isKeyPressed(Keys.SPACE) && lastShoot > 0.2f)
		{
			shoot.shoot(player.getPosition(),player.getPlayerDirection());
			lastShoot = 0f;
		}
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		lastShoot += Gdx.graphics.getDeltaTime() / 5;
		moveTimer += Gdx.graphics.getDeltaTime() / 2;
		Timer += Gdx.graphics.getDeltaTime() / 5;
		handleInput();
		player.update(dt);
		shoot.update(player.getPosition());
		if (moveTimer > 0.02f)
		{
			tank.update();
			moveTimer  = 0f;
		}
		if (Timer > 0.2f)
		{
			tank.getRandom();
			Timer  = 0f;
		}
	}

	@Override
	public void render() {
		renderer.render();
		cam.update();
		renderer.setView(cam);
		
		sb.begin();
		player.draw();
		tank.draw();
		shoot.draw();
		sb.end();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
