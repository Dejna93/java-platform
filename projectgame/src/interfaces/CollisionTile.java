package interfaces;

import com.badlogic.gdx.math.Vector2;

public interface CollisionTile {
	
	public boolean isCellBlocked(float x, float y);
	boolean collidesRight(Vector2 position);
	boolean collidesLeft(Vector2 position);
	boolean collidesTop(Vector2 position);
	boolean collidesDown(Vector2 position);
}
