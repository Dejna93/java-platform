package handlers;

public class GameKeys {
	
	private static boolean[] keys;
	private static boolean[] pkeys;
	
	private static final int KEYS = 5;
		public static final int UP = 0;
		public static final int DOWN = 1;
		public static final int LEFT = 2;
		public static final int RIGHT = 3;
		public static final int SPACE = 4;
	
	static{
		keys= new boolean[KEYS];
		pkeys = new boolean[KEYS];
	}
		
	public static void update()
	{
		for (int i=0; i < KEYS ; i++)
		{
			pkeys[i] = keys[i];
		}
	}
	
	public static void setKey(int k, boolean b)
	{
		keys[k] = b;
	}
	
	public static boolean isDown(int k)
	{
		return keys[k];
	}
	public static boolean isPressed(int k)
	{
		return keys[k] && !pkeys[k];
	}
}
