package handlers;

import java.util.Stack;

import main.Game;
import states.GameState;
import states.PlaySingle;

public class GameStateManager {

	private Game game;
	private Stack<GameState> gameStates; //stos do roznych poziomow 
	
	//zmienne do stanow gry
	public static final int MENU = 53132;
	public static final int PLAY = 41232;
	
		
	
	public GameStateManager(Game game)
	{
		this.game = game;
		gameStates = new Stack<GameState>();
		pushState(PLAY);
	}
	
	public void update(float dt)
	{
		gameStates.peek().update(dt);
	}
	public void render()
	{
		gameStates.peek().render();
	}
	public Game game() {return game;}
	
	private GameState getState(int state)
	{
		//if (state == MENU)
		//	return new Menu(this);
		if (state == PLAY)
			return new PlaySingle(this);
		return null;
	}
	
	public void setState (int state)
	{
		popState();
		pushState(state);
	}
	public void pushState(int state)
	{
		gameStates.push(getState(state));
	}
	public void popState()
	{
		GameState gam = gameStates.pop();
		gam.dispose();
	}
}
