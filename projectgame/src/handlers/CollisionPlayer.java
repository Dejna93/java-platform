package handlers;

import interfaces.CollisionTile;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;



public class CollisionPlayer implements CollisionTile {
	
	private TiledMapTileLayer collisionLayer;
	private float width,height;
	private String Key;
	public CollisionPlayer(TiledMapTileLayer collisionLayer,float width,float height,String key) {
		// TODO Auto-generated constructor stub
		this.collisionLayer = collisionLayer;
		this.width = width;
		this.height = height;
		this.Key = key;
	}

	
	
	@Override
	public boolean isCellBlocked(float x, float y) {
			Cell cell = collisionLayer.getCell((int) (x / collisionLayer.getTileWidth()), (int) (y / collisionLayer.getTileHeight()));
		return cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey(Key);
	
	}

	@Override
	public boolean collidesRight(Vector2 position) {
		for(float step = 0; step < height; step += collisionLayer.getTileHeight() / 2)
			if(isCellBlocked(position.x + width, position.y + step +5))
				return true;
		return false;
	}

	@Override
	public boolean collidesLeft(Vector2 position) {
		for(float step = 0; step < height; step += collisionLayer.getTileHeight() / 2)
			if(isCellBlocked(position.x, position.y + step +5))
				return true;
		return false;
	}

	@Override
	public boolean collidesTop(Vector2 position) {
		for(float step = 0; step < height; step += collisionLayer.getTileWidth() / 2)
			if(isCellBlocked(position.x + step +5, position.y + height))
				return true;
		return false;
	}

	@Override
	public boolean collidesDown(Vector2 position) {
		// TODO Auto-generated method stub
		for(float step = 0; step <height; step += collisionLayer.getTileWidth() / 2)
			if(isCellBlocked(position.x + step +5, position.y))
				return true;
		return false;
	}

}
