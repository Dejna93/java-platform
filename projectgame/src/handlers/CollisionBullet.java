/**
 * 
 */
package handlers;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;

import interfaces.CollisionTile;

/**
 * @author callo_000
 *
 */
public class CollisionBullet implements CollisionTile {

	private TiledMapTileLayer collisionLayer;
	private float width,height;
	private String Key;
	private TiledMap map;
	public CollisionBullet(TiledMapTileLayer collisionLayer,float width,float height,String key,TiledMap map) {
		// TODO Auto-generated constructor stub
		this.collisionLayer = collisionLayer;
		this.width = width;
		this.height = height;
		this.Key = key;
		this.map = map;
	}

	
	
	@Override
	public boolean isCellBlocked(float x, float y) {
		
		Cell cell = collisionLayer.getCell((int) (x / collisionLayer.getTileWidth()), (int) (y / collisionLayer.getTileHeight()));
		return cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey(Key);
	
	}

	@Override
	public boolean collidesRight(Vector2 position) {
		for(float step = 0; step < height; step += collisionLayer.getTileHeight() / 2)
			if(isCellBlocked(position.x + width, position.y + step))
				{	
				System.out.println("S");
				return true;
				}
		return false;
	}

	@Override
	public boolean collidesLeft(Vector2 position) {
		for(float step = 0; step < height; step += collisionLayer.getTileHeight() / 2)
			if(isCellBlocked(position.x, position.y + step))
			{
				System.out.println("S");
				return true;
			}
		return false;
	}

	@Override
	public boolean collidesTop(Vector2 position) {
		for(float step = 0; step < height; step += collisionLayer.getTileWidth() / 2)
			if(isCellBlocked(position.x + step, position.y + height))
			{		
				System.out.println("S");
				return true;
				}
		return false;
	}

	@Override
	public boolean collidesDown(Vector2 position) {
		// TODO Auto-generated method stub
		for(float step = 0; step <height; step += collisionLayer.getTileWidth() / 2)
			if(isCellBlocked(position.x + step, position.y))
			{	
				System.out.println("S");
				return true;
				}
		return false;
	}

}
