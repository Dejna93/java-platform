package entities;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Tank extends Enemy {
	
	private SpriteBatch sb;
	private int step=0;
	public Tank(SpriteBatch sb)
	{
		this.sb = sb;
		health = 1;
		this.x = 100;
		this.y = 100;
		sprite = new Sprite(new Texture("assets/texture/enemy_tank_1.png"));
		this.width = Gdx.graphics.getWidth();
		this.height = Gdx.graphics.getHeight();
		setPosition(x, y);
	}

	public void getRandom()
	{
		Random direction = new Random();
		step = direction.nextInt(100);
	}
	public void randMove()  //bardzo prymitywne poruszanie do testow kolizji
	{
		
			// {0,1,2,3} top down left right
		if (step >=0 && step <25)
			y +=2;
		if (step >=25 && step <50)
			y -=2;
		if (step >=50 && step <75)
			x -=2;
		if (step >=75 && step <100)
			x+=2;
		
		

			
		if (x < 0 )
		{
			x=0;
			
		}
		if(x>width)
		{
			x=width;

		}
		if (y<0)
		{
			y=0;
		}
		if (y> height)
		{
			y=height;
		}
		setPosition(x, y);
	}
	
	public void update()
	{
		randMove();
		
	}
	private void setPosition( float x , float y)
	{
		sprite.setPosition(x, y);
	}
	public void draw()
	{
		sprite.draw(sb);
	}
}
