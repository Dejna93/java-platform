package entities;


import handlers.CollisionBullet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;

public class Shoot extends Vars {

	
	private float x,y;
	private Vector2 v;
	
	private float speed;
	
	private Sprite sprite;
	private SpriteBatch sb;
	//private boolean hit;
	private boolean shooted;
	private Direction shootDirection;
	
	
	private TiledMapTileLayer collisionLayer;
	private TiledMap map;
	private float WIDTH,HEIGHT;
	
	private Vector2 playerPosition;
	private boolean visible;
	private CollisionBullet c;
	public Shoot(SpriteBatch sb,TiledMapTileLayer collisionLayer,TiledMap map)
	{
		this.sb = sb;
		sprite = new Sprite(new Texture("assets/texture/shoot.png"));	
		setShootDirection(Direction.Default);
		shooted = false;
		visible = false;
		WIDTH = Gdx.graphics.getWidth();
		HEIGHT = Gdx.graphics.getHeight();
		this.collisionLayer= collisionLayer;
		this.map= map;
		
		speed  = 5;
		v = new Vector2(0,0);

	}
	
	public void shoot(Vector2 playerPosition, Direction playerD )
	{
		if (!shooted)
		{
		setShootDirection(playerD);
		x = playerPosition.x+6;
		y = playerPosition.y+13;
		shooted = true;
		visible = true; 
		}
	}
	public void move(Vector2 v)
	{

		x +=v.x;
		y +=v.y;
	}
	
	//dodanie funkcji do kolizji bulleta z usuwaniem go
	public void update(Vector2 playerPosition)
	{
		this.playerPosition = playerPosition;
		show();
		if(shooted)
		{
			switch (shootDirection) {
			case Up:
				move(new Vector2(0,speed));
				break;
			case Down:
				move (new Vector2(0,-speed));
				break;
			case Left:
				move(new Vector2(-speed,0));
				break;
			case Right:
				move(new Vector2(speed,0));
				break;
			case Default:
				move(new Vector2(0,0));
				break;
			default:
				break;
			}
		}
		else
		{
			x = playerPosition.x;
			y = playerPosition.y;
		}
		//show();
		sprite.setPosition(x, y);
		clean(); 

		}
	public void show()
	{	try{
		for (float step =0 ; step < getHeight(); step +=collisionLayer.getTileHeight()/2)
		{
			Cell cell = collisionLayer.getCell((int)(x / collisionLayer.getTileWidth()),(int)(y/collisionLayer.getTileHeight()));
			if(cell.getTile().getProperties().containsKey("shootable") && cell != null)
			{
				setVelocity(0, 0);
				setPosition(playerPosition);
				shooted = false;
				cell.setTile(map.getTileSets().getTile(0));
				break;			
			}
		}
	}
	catch(Exception e)
	{
	}
	}
	
	private void setPosition(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	private void setPosition(Vector2 pos)
	{
		this.x = pos.x;
		this.y = pos.y;
	}
	private void setVelocity(float x , float y)
	{
		v.x = x;
		v.y = y;
	}
	private void clean()
	{
		if (x < 0 )
			shooted = false;
		if(x>WIDTH)
			shooted = false;
		if (y<0)
			shooted = false;
		if (y> HEIGHT)
			shooted = false;
	}
	
	private float getWidth()
	{
		return sprite.getWidth();
	}
	private float getHeight() {
		return sprite.getHeight();
	}
	
	
	public void draw()
	{
		if(visible)
			sprite.draw(sb);
	
	}
	public Direction getShootDirection() {
		return shootDirection;
	}

	public void setShootDirection(Direction shootDirection) {
		this.shootDirection = shootDirection;
	}
}
