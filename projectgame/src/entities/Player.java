package entities;

import handlers.CollisionPlayer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import com.badlogic.gdx.math.Vector2;

public class Player extends Vars{

	private Sprite sprite;
	private SpriteBatch sb;
	private String blockedKey;
	
	private CollisionPlayer collision;
	//skierowanie czolgu
	private Direction playerDirection;
	
	private TiledMapTileLayer collisionLayer;
	
	public Player(SpriteBatch sb,TiledMapTileLayer collisionLayer)
	{
		x = 50;
		y = 50;
		blockedKey = "block";
		this.collisionLayer = collisionLayer;
		this.sb = sb;
		sprite = new Sprite( new Texture(Gdx.files.internal("assets/texture/player_top.png")));
		sprite.setOrigin(13,16);

		speed = 5;
		
		collision = new CollisionPlayer(collisionLayer, getWidth(), getHeight(), blockedKey);
		
		setPlayerDirection(Direction.Up);
	}
	 
	public void setLeft()
	{
		move(-speed , 0);
		setPlayerDirection(Direction.Left);
		sprite.setRotation(90);
	}
	public void setRight()
	{
		move(speed ,0 );
		setPlayerDirection(Direction.Right);
		sprite.setRotation(-90);
	}
	public void setUp()
	{
		move(0,speed );
		setPlayerDirection(Direction.Up);
		sprite.setRotation(0);
	}
	public void setDown()
	{
		move(0,-speed );
		setPlayerDirection(Direction.Down);
		sprite.setRotation(180);
	}
	public void viewBlocked()
	{
		
	}
	private void move(float vx ,float vy)
	{
		float oldX=x,oldY=y;
	
		boolean collisionX = false,collisionY = false;
		
		if (vx <0)
		{
			collisionX = collision.collidesLeft(new Vector2(getX(),getY()));
			if(collisionX)
			{
				vx =0;
				x = oldX;
			}
		}
		else if (vx>0)
		{
			collisionX = collision.collidesRight(new Vector2(getX(),getY()));
			if(collisionX)
			{
				vx =0;
				x = oldX;
			}
		}
		
		if (vy < 0)
		{
			collisionY = collision.collidesDown(new Vector2(getX(),getY()));
			if (collisionY)
			{
				y = oldY;
				vy = 0;
			}
		}
		else if ( vy > 0)
			{
			collisionY = collision.collidesTop(new Vector2(getX(),getY()));
			if (collisionY)
			{
				y = oldY;
				vy =0;
			}
			}
		if (x < 0 )
		{
			x=0;
			vx=0;
		}
		if(x>WIDTH-getWidth())
		{
			x=WIDTH-getWidth();
			vx=0;
		}
		if (y<0)
		{
			y=0;
			vy=0;
		}
		if (y> HEIGHT-getHeight())
		{
			y=HEIGHT-getHeight();
			vy=0;
		}
			
		
		this.x += vx;
		this.y += vy;
	}

	
	public void update(float dt)
	{	
	
		sprite.setPosition(x,y);
	}
	
	public void draw()
	{
		sprite.draw(sb);
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	private int getHeight()
	{
		return (int) sprite.getHeight();	
	}
	private float getWidth() {
		// TODO Auto-generated method stub
		return (int) sprite.getWidth();
	}
	public Direction getPlayerDirection() {
		return playerDirection;
	}

	public void setPlayerDirection(Direction playerDirection) {
		this.playerDirection = playerDirection;
	}

	public Vector2 getPosition() {
		// TODO Auto-generated method stub
		Vector2 pos = new Vector2(x,y);
		return pos;
	}

	
}
