package entities;

import com.badlogic.gdx.Gdx;

public class Vars {
	
	protected float x;

	protected final float WIDTH=Gdx.graphics.getWidth();
	protected final float HEIGHT=Gdx.graphics.getHeight();

	protected float y;
	
	protected float speed;
	
	protected float dx;
	protected float dy;
	
	protected enum Direction {
		 Up , Down	,Left , Right , Default
	};
}
